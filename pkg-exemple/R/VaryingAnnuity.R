### pkg: <package title>
###
### Computation of the present value of immediate and due annuities
### varying in arithmetic progression.
###
### Reference: Kellison, S. G. (1991), The Theory of Interest,
### Irwin/McGraw-Hill, ISBN 978-025609150-2
###
### AUTHORS: Jean-Christophe Langlois, Vincent Goulet <vincent.goulet@act.ulaval.ca>
### LICENSE: GPL 2 or later.

incr.pres.value <- function(n, i,  due = FALSE)
{
    stopifnot(exprs = {
        "interest rate must be positive" = all(i >= 0)
        "number of periods must be strictly positive" = all(n > 0)
    })

    ## We need to treat specially the case where i = 0 since the
    ## general formula does not apply.
    ##
    ## First, create a vector to hold present values.
    value <- numeric(max(length(n), length(i)))

    ## Set the present value for cases with i = 0.
    wi <- i == 0
    value[wi] <- n[wi] * (n[wi] + 1)/2

    # Perpetuity
    wp <- n == Inf
    value[wp] <- (1 + i[wp])/i[wp]^2

    ## Now the cases with i > 0
    w <- !(wi | wp)                     # needed positions
    iw <- i[w]                          # needed interest rates
    nw <- n[w]                          # needed durations
    value[w] <- (pres.value(nw, iw, due = TRUE) - nw/(1 + iw)^nw)/iw

    ## Multiply by (1 + i) for annuity due.
    if (due)
        value[w] <- (1 + iw) * value[w]

    value
}

decr.pres.value <- function(n, i, due = FALSE)
{
    stopifnot(exprs = {
        "interest rate must be positive" = all(i >= 0)
        "number of periods must be strictly positive" = all(n > 0)
        "Perpetuities are not defined" = all(n < Inf)
    })

    ## We need to treat specially the case where i = 0 since the
    ## general formula does not apply.
    ##
    ## First, create a vector to hold present values.
    value <- numeric(max(length(n), length(i)))

    ## Set the present value for cases with i = 0.
    w <- i == 0
    value[w] <- n[w] * (n[w] + 1)/2

    ## Now the cases with i > 0.
    w <- !w                             # reverse positions
    iw <- i[w]                          # needed interest rates
    nw <- n[w]                          # needed durations
    value[w] <- (nw - pres.value(nw, iw))/iw

    ## Multiply by (1 + i) for annuity due.
    if (due)
        value[w] <- (1 + iw) * value[w]

    value
}
