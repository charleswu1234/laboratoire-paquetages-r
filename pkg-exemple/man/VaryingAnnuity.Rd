\name{VaryingAnnuity}
\alias{VaryingAnnuity}
\alias{incr.pres.value}
\alias{decr.pres.value}
\title{Present Value of Increasing and Decreasing Annuities}
\description{
  Present value of immediate or due annuities with payment increasing or
  decreasing in arithmetic progression, or increasing perpetuities.
}
\usage{
incr.pres.value(n, i, due = FALSE)
decr.pres.value(n, i, due = FALSE)
}
\arguments{
  \item{n}{<insert description of argument here>}
  \item{i}{<insert description of argument here>}
  \item{due}{<insert description of argument here>}
}
\details{
  The present value of \eqn{n}-payment, end-of-period (or
  \emph{immediate}), annuity with payments increasing in arithmetic
  progression starting from \eqn{1} is
  \deqn{v + 2v^2 + 3v^3 + \dots + nv^n = \frac{\ddot{a} - n v^n}{i},}{%
    v + 2 v^2 + 3 v^3 + ... + n v^n = (A - n v^n)/i,}
  where \eqn{\ddot{a}}{A} is the present value of an \eqn{n}-period
  level annuity due, and \eqn{v = 1/(1 + i)}.

  The present value of \eqn{n}-payment, end-of-period (or
  \emph{immediate}), annuity with payments decreasing in arithmetic
  progression starting from \eqn{n} is
  \deqn{nv + (n - 1) v^2 + (n - 2) v^3 + \dots + v^n = \frac{n - a}{i},}{%
    nv + (n - 1) v^2 + (n - 2) v^3 + ... + v^n = (n - a)/i,}
  where \eqn{a} is the present value of an \eqn{n}-period level annuity
  immediate, and \eqn{v = 1/(1 + i)}.

  For beginning-of-period (or \emph{due}) annuities, the \eqn{i} in the
  denominators above is replaced by \eqn{d = 1/(1 + i)}.
}
\value{
  Vector of present values.
}
\seealso{
  \code{\link{Annuity}} for the valuation of level payments annuities.
}
\examples{
## 10-period increasing annuity with an interest rate of 5%
incr.pres.value(10, 0.05)

## Increasing perpetuity.
incr.pres.value(Inf, 0.05)

## 20-period decreasing annuity with an interest rate of 4%
incr.pres.value(20, 0.04)
}
