### -*-Makefile-*- pour préparer "Conception de paquetages R"
##
## Copyright (C) 2021-2022 Vincent Goulet
##
## 'make pdf' compile le document maitre avec XeLaTeX.
##
## 'make contrib' crée le fichier COLLABORATEURS.
##
## 'make update-copyright' met à jour l'année de copyright dans toutes
## les sources du document
##
## 'make zip' crée la distribution du matériel pédagogique.
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .zip.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make pdf' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet "Conception de paquetages R"
## https://gitlab.com/vigou3/laboratoire-paquetages-r


## Principaux fichiers
MASTER = laboratoire-paquetages-r.pdf
ARCHIVE = ${MASTER:.pdf=.zip}
README = README.md
NEWS = NEWS
COLLABORATEURS = COLLABORATEURS
CONTRIBUTING = CONTRIBUTING.md
LICENSE = LICENSE

## Le document maitre dépend de tous les fichiers .tex autres que
## lui-même.
TEXFILES = $(addsuffix .tex, \
                       $(filter-out $(basename ${MASTER}),\
                                    $(basename $(wildcard *.tex))))

## L'archive contient une ébauche de paquetage R dont il faut
## préserver la structure.
PKGDIR = pkg-exemple
PKGFILES = ${PKGDIR}/DESCRIPTION ${PKGDIR}/NAMESPACE \
	   $(wildcard ${PKGDIR}/R/*.R) \
	   $(wildcard ${PKGDIR}/man/*.Rd) \
	   $(wildcard ${PKGDIR}/tests/*.R)

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep -m 1 "\\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep -m 1 "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep -m 1 "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep -m 1 "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Auteurs à exclure du fichier COLLABORATEURS (regex)
OMITAUTHORS = Vincent Goulet|Inconnu|unknown

## Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
CP = cp -p
CPIO = cpio -pdm
RM = rm -rf

## Dossier temporaire pour construire l'archive
BUILDDIR = builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: pdf

FORCE:

${MASTER}: ${MASTER:.pdf=.tex} ${TEXFILES} $(wildcard images/*)
	${TEXI2DVI} ${MASTER:.pdf=.tex}

${COLLABORATEURS}: FORCE
	(git log --pretty="%an%n") | sort | uniq | \
	  grep -v -E "${OMITAUTHORS}" | \
	  awk 'BEGIN { print "Les personnes dont le nom [1] apparait ci-dessous ont contribué à\nl'\''amélioration de «${TITLE}»." } \
	       { print $$0 } \
	       END { print "\n[1] Noms tels qu'\''ils figurent dans le journal du dépôt Git\n    ${REPOSURL}" }' > ${COLLABORATEURS}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: contrib
contrib: ${COLLABORATEURS}

.PHONY: release
release: update-copyright zip check-status create-release create-link publish

.PHONY: update-copyright
update-copyright: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES} ${SHARE}
	for f in $?; \
	    do sed -E '/^(#|%)* +Copyright \(C\)/s/-20[0-9]{2}/-$(shell date "+%Y")/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: zip
zip: ${MASTER} ${PKGFILES} ${README} ${NEWS} ${LICENSE} ${COLLABORATEURS} ${CONTRIBUTING}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	mkdir ${BUILDDIR}
	touch ${BUILDDIR}/${README} && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Auteur/ { printf("## Édition\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${README}
	${CP} ${MASTER} ${NEWS} ${LICENSE} ${COLLABORATEURS} ${CONTRIBUTING} \
	      ${BUILDDIR}
	echo ${PKGFILES} | tr " " "\n" | ${CPIO} ${BUILDDIR}
	cd ${BUILDDIR} && zip --filesync -r ../${ARCHIVE} *
	if [ -e ${COLLABORATEURS} ]; then ${RM} ${COLLABORATEURS}; fi
	${RM} ${BUILDDIR} 

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "----- Checking status of working directory... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "uncommitted changes in repository; not creating release"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "----- Checking if a release already exists... "; \
	    http_code=$$(curl -I ${APIURL}/releases/${TAGNAME} 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "yes"; \
	        printf "%s\n" "using the existing release"; \
	    else \
	        printf "%s\n" "no"; \
	        printf "%s" "Creating release on GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "done"; \
	    fi; \
	}

.PHONY: create-link
create-link: create-release
	@{ \
	    printf "%s" "----- Adding asset to the release... "; \
	    url=$$(curl --form "file=@${ARCHIVE}" \
	                --header "PRIVATE-TOKEN: ${OAUTHTOKEN}"	\
	                --silent \
	           ${APIURL}/uploads \
	           | awk -F '"' '{ print $$8 }'); \
	    curl --request POST \
	         --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	         --data name="${ARCHIVE}" \
	         --data url="${REPOSURL}$${url}" \
	         --data link_type="package" \
	         --output /dev/null --silent \
	         ${APIURL}/releases/${TAGNAME}/assets/links; \
	    printf "%s\n" "done"; \
	}

.PHONY: publish
publish:
	@echo ----- Publishing the web page...
	git checkout pages && \
	  ${MAKE} && \
	  git checkout master
	@echo ----- Done publishing

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${TEXFILES}
	@echo ----- Checking urls in sources...
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq | sed -E "s/.*/\'&\'/"))
	for u in ${url}; \
	    do if curl --output /dev/null --silent --head --fail --max-time 5 $$u; then \
	        echo "URL exists: $$u"; \
	    else \
		echo "URL does not exist (or times out): $$u"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      ${MASTER:.html=.pdf} \
	      ${ARCHIVE} \
	      ${COLLABORATEURS} \
	      *.aux *.log *.snm *.nav *.vrb
